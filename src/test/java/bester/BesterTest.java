package bester;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class BesterTest {
    @Test
    public void oneRectangleItSelfIsTheBest() {
        Bester bester = new Bester();
        Rectangle rectangle = new Rectangle(3, 4);
        List<Betterable> rectangleList = new ArrayList<>();
        rectangleList.add(rectangle);

        assertEquals(bester.getBest(rectangleList), rectangle);
    }

    @Test
    public void checkIfRectangleWithLargestAreaIsTheBest() {
        Bester bester = new Bester();
        Rectangle rectangle1 = new Rectangle(3, 4);
        Rectangle rectangle2 = new Rectangle(5, 4);
        List<Betterable> rectangleList = new ArrayList<>();
        rectangleList.add(rectangle1);
        rectangleList.add(rectangle2);

        assertEquals(bester.getBest(rectangleList), rectangle2);
    }
    @Test
    public void checkIfCookieWithHighestNumberOfChocolateChipsIsTheBest() {
        Bester bester = new Bester();
        Cookie cookie1 = new Cookie(3);
        Cookie cookie2 = new Cookie(5);
        List<Betterable> cookiesList = new ArrayList<>();
        cookiesList.add(cookie1);
        cookiesList.add(cookie2);

        assertEquals(bester.getBest(cookiesList), cookie2);
    }

    @Test
    public void checkIfExceptionIsThrownIfTryingToCompareTwoDifferentBetterableObjects(){
        Bester bester = new Bester();
        Cookie cookie = new Cookie(3);
        Rectangle rectangle = new Rectangle(5,3);
        List<Betterable> betterablesList = new ArrayList<>();
        betterablesList.add(cookie);
        betterablesList.add(rectangle);

        assertThrows(UnsupportedOperationException.class,()->bester.getBest(betterablesList));
    }
}
