package bester;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CookieTest {
    @Test
    public void checkIfBetterThanIsReturningCookieWithHighestNumberOfChocolateChips(){
        Cookie cookie1 = new Cookie(3);
        Cookie cookie2 = new Cookie(5);
        assertTrue(cookie1.betterThan((Betterable)(cookie2)));
    }
}
