package bester;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RectangleTest {
    @Test
    public void areaShouldBeEqualToLengthyMultipliedByBreadth() throws Exception {
        assertEquals(6.0, new Rectangle(2, 3).area());
        assertEquals(0.0, new Rectangle(0, 1).area());
        assertEquals(3.75, new Rectangle(1.5, 2.5).area());
    }

    @Test
    public void checkIfBetterThanIsReturningRectangleWithLargestArea(){
        Rectangle rectangle1 = new Rectangle(3, 3);
        Rectangle rectangle2 = new Rectangle(4, 4);

        assertTrue(rectangle1.betterThan(rectangle2));
    }

}