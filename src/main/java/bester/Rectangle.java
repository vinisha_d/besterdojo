package bester;

import java.util.Objects;

public class Rectangle implements Betterable {
    private double length;
    private double breadth;

    public Rectangle(double breadth, double length) {
        this.breadth = breadth;
        this.length = length;
    }

    public double area() {
        return length * breadth;
    }

    @Override
    public boolean betterThan(Betterable rectangle2) {
        return this.area() < ((Rectangle) rectangle2).area();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        return area() == ((Rectangle) o).area();
    }

    @Override
    public int hashCode() {
        return Objects.hash(length, breadth);
    }

}
