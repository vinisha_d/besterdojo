package bester;

public interface Betterable {
    boolean betterThan(Betterable betterable);
}
