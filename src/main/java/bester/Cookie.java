package bester;

public class Cookie implements Betterable {

    private int numberOfChocolateChips;

    public Cookie(int numberOfChocolateChips) {
        this.numberOfChocolateChips = numberOfChocolateChips;
    }

    @Override
    public boolean betterThan(Betterable otherCookie) {
        return this.numberOfChocolateChips < ((Cookie) otherCookie).numberOfChocolateChips;
    }
}
