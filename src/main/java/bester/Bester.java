package bester;

import java.util.List;

public class Bester {
    public Betterable getBest(List<Betterable> betterablesList) {
        int FIRST_BETTERABLE_INDEX = 0;
        Betterable best =betterablesList.get(FIRST_BETTERABLE_INDEX);
        for (Betterable betterable :betterablesList) {
            if( betterable.getClass() != best.getClass() ){
                throw new UnsupportedOperationException();
            }
            if (best.betterThan(betterable)) {
                best = betterable;
            }
        }
        return best;
    }
}